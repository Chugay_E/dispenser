# Flux or paste dispenser project.

Target MCU: **STM32L011D3P6**   

---

[CMake](https://cmake.org/)   
[MinGW](https://sourceforge.net/projects/mingw/)   
[GCC arm-none-eabi-*** toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)   
[3D-print mechanism](https://grabcad.com/library/flux-or-paste-dispenser-1)   

---

-   SSD1306 OLED 0,96";   
-   Two buttons for settings;   
-   ULN2003 for step motor.   

---

Functions:
-   String menu;   
-   Feed dose after button press;   
-   Setting steps quantity for one feed move;   
-   Always move fwd/reverse mode;   
-   Setting delay for feed after press button.   

---

## MCU pinout:
| PIN |  PORT  | GPIO |          FUNCTION          |
|----:|:------:|:----:|:---------------------------|
|  1  | BOOT0  | SYS  | BOOT0                      |
|  2  | PC14   | IN   | Button [A]                 |
|  3  | PC15   | IN   | Button [B]                 |
|  4  | NRST   | SYS  | RESET                      |
|  5  | PA0    | OUT  | Step motor, phase A        |
|  6  | PA1    | OUT  | Step motor, phase B        |
|  7  | PA4    | OUT  | Step motor, phase C        |
|  8  | PA7    | OUT  | Step motor, phase D        |
|  9  | VSS    | SYS  | GND                        |
| 10  | VDD    | SYS  | 3.3V                       |
| 11  | PA9    | I2C  | I2C, SCL (for display)     |
| 12  | PA10   | I2C  | I2C, SDA (for display)     |
| 13  | PA13   | SWD  | SWD, DIO                   |
| 14  | PA14   | SWD  | SWD, CLK                   |

---

## HELP:

-	CMake command template:   
```cmake -S <path-to-source> -B <path-to-build>```   

-	Make command template:   
```mingw32-make -f <path-to-build-makefile>```   
