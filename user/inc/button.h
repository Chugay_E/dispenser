/**
 * @file		button.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		05.12.2022
 * @brief		Функции инициализации входов GPIO и обработки нажатия кнопок для проекта диспенсера.
 */

#ifndef BUTTON_H_
#define BUTTON_H_

/*----------------------------------------------------------------- Includes: */
#include "stm32l0xx.h"

/*-------------------------------------------------------------------- Macro: */
#define BTN_A	(GPIOC->IDR & GPIO_IDR_ID14)
#define BTN_B	(GPIOC->IDR & GPIO_IDR_ID15)

typedef union {
    struct {
        volatile uint8_t a_click   :1;
        volatile uint8_t b_click   :1;
        volatile uint8_t a_long    :1;
        volatile uint8_t b_long    :1;
        volatile uint8_t a_release :1;
        volatile uint8_t b_release :1;
        volatile uint8_t reserve   :2;
    };
    volatile uint8_t value;
} BTN_SIGNAL;

/*------------------------------------------------------- Function prototype: */
/**
 * @brief   Инициализация GPIO как входы кнопок.
 * @param   Нет.
 * @retval  Нет.
 */
void init_button_pin (void);

/**
 * @brief   Обработка нажитай кнопок, выдача
 * @param   Нет.
 * @retval  Сигнал о нажатии или удержании кнопки (см. структуру BTN_SIGNAL).
 */
uint8_t button_debounce (void);

#endif /* BUTTON_H_ */
