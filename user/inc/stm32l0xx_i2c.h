/**
 * @file		stm32l0_i2c.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		21.03.2022
 * @brief		Работа с интерфейсом I2C МК STM32L011D3P6.
 */

#ifndef STM32L0_I2C_H_
#define STM32L0_I2C_H_

/*----------------------------------------------------------------- Includes: */
#include "main.h"

/*------------------------------------------------------------------ Defines: */

/*------------------------------------------------------- Function prototype: */
/**
 * @brief   Инициализация I2C1.
 * @param   Нет.
 * @retval  Нет.
 */
void I2C1_init(void);

/**
 * @brief   Отправка данных по шине I2C1.
 * @param   addr_slave - адрес ведомого устройства на шине данных.
 * @param   addr_word  - адрес слова ведомого устройства.
 * @param   size       - кол-во передаваемых байт.
 * @param   *data      - указатель на массив, из которго передаются данные.
 * @retval  Нет.
 */
void I2C1_write(uint8_t addr_slave, uint8_t addr_word, uint8_t size, uint8_t *data);

/**
 * @brief   Чтение данных по шине I2C1.
 * @param   addr_slave - адрес ведомого устройства на шине данных.
 * @param   addr_word  - адрес слова ведомого устройства (не реализовано!).
 * @param   size       - кол-во считываемых байт.
 * @param   *data      - указатель на массив, для записи входящих данных.
 * @retval  Нет.
 */
void I2C1_read (uint8_t addr_slave, uint8_t addr_word, uint8_t size, uint8_t *data);


#endif /* STM32L0_I2C_H_ */
