/**
 * @file		main.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		29.11.2022
 * @brief       Проект для диспенсера паяльной пасты на базе
 *              микроконтроллера STM32F011D3P6.
 */

#ifndef MAIN_H_
#define MAIN_H_

/*----------------------------------------------------------------- Includes: */
#include "stm32l0xx.h"
#include "ssd1306.h"
#include "stepper.h"
#include "button.h"

/*-------------------------------------------------------------------- Macro: */

/*------------------------------------------------------- Function prototype: */

#endif /* MAIN_H_ */
