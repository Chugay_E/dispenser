/**
 * @file		stepper.h
 * @author		Chugay E.A.
 * @version		0.01
 * @date		29.11.2022
 * @brief
 */

#ifndef STEPPER_H_
#define STEPPER_H_

/*----------------------------------------------------------------- Includes: */
#include "main.h"

/*-------------------------------------------------------------------- Macro: */

/*------------------------------------------------------- Function prototype: */
/**
 * @brief	Инициализация выводов для управления двигателем.
 * @param	Нет.
 * @retval	Нет.
 */
void init_stepper (void);

/**
 * @brief	Установка скорости работы двигателя.
 * @param	speed - число тиков таймера на шаг. От 1 до 16.
 * @retval	Нет.
 */
void stepper_set_speed (uint8_t speed);

/**
 * @brief	Запуск задания для двигателя.
 * @param	step_num - число шагов, через которое произойдет остановка.
 * @retval	Нет.
 */
void stepper_start (int32_t step_num);

/**
 * @brief	Остановка двигателя.
 * @param	Нет.
 * @retval	Нет.
 */
void stepper_stop (void);

/**
 * @brief	Возвращает статус, в работе или остановлен.
 * @param	Нет.
 * @retval	1 - работает, 0 - остановлен.
 */
uint8_t stepper_get_state (void);

/**
 * @brief	Возвращает значение уставки скорости.
 * @param	Нет.
 * @retval	От 1 до 16.
 */
uint8_t stepper_get_speed (void);

/**
 * @brief	Переключение фаз двигателя, вызывается в прерывании таймера.
 * @param	Нет.
 * @retval	Нет.
 */
void stepper_handler (void);



#endif /* STEPPER_H_ */
