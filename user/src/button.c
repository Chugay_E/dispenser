/**
 * @file		button.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		05.12.2022
 * @brief		Функции инициализации входов GPIO и обработки нажатия кнопок для проекта диспенсера.
 */

/*----------------------------------------------------------------- Includes: */
#include "button.h"

/*-------------------------------------------------------------------- Macro: */
#define BTN_QUANTITY	((uint8_t)2)
#define DEBOUNCE_TIME	((uint8_t)2)
#define BTN_LONG		((uint8_t)150)

/*---------------------------------------------------------- Extern variable: */

/*--------------------------------------------------------- Public variables: */

/*--------------------------------------------------------- Private variable: */
static uint8_t btn_cnt[BTN_QUANTITY] = {0, 0};

/*----------------------------------------------------------------- Function: */
//	Init buttons GPIO
void init_button_pin (void)
{
    RCC->IOPENR |= RCC_IOPENR_GPIOCEN;
    GPIOC->MODER &= ~(GPIO_MODER_MODE14 | GPIO_MODER_MODE15);   //	PC14, PC15 - BTN.
}



//	Button handler
uint8_t button_debounce (void)
{
    static volatile uint8_t btn_flag = 0;

    uint8_t input = 0x00;
    if (BTN_A) input |= (1 << 0);
    if (BTN_B) input |= (1 << 1);

    BTN_SIGNAL btn;
    btn.value = 0x00;

    for(uint8_t i = 0; i < BTN_QUANTITY; i++) {
        if ((input & (1 << i)) && !(btn_flag & (1 << i))) {
            btn_flag |= (1 << i);
        } else if ((input & (1 << i)) && (btn_flag & (1 << i))) {
            if (btn_cnt[i] <= BTN_LONG) {
                btn_cnt[i]++;
            }
            if (btn_cnt[i] == BTN_LONG) {
                btn.value |= (1 << (i + BTN_QUANTITY));
            } else {
                btn.value &= ~(1 << (i + BTN_QUANTITY));
            }
            (btn_cnt[i] == DEBOUNCE_TIME) ? (btn.value |= (1 << i)) : (btn.value &= ~(1 << i));
        } else if (!(input & (1 << i)) && (btn_flag & (1 << i))) {
            if ((btn_cnt[i] > DEBOUNCE_TIME) && (btn_cnt[i] < BTN_LONG)) btn.value |= (1 << (i + (BTN_QUANTITY << 1)));
            btn.value &= ~((1 << i) | (1 << (i + BTN_QUANTITY)));
            btn_flag &= ~(1 << i);
            btn_cnt[i] = 0x00;
        }
    }

    return btn.value;
}
