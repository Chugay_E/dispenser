/**
 * @file		main.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		29.11.2022
 * @brief       Проект для диспенсера паяльной пасты на базе
 *              микроконтроллера STM32F011D3P6.
 */

/*----------------------------------------------------------------- Includes: */
#include "main.h"

/*-------------------------------------------------------------------- Macro: */
#define TIMEOUT_VALUE       ((uint16_t)500)
#define IWDG_START          (uint32_t)(0x0000CCCC)
#define IWDG_WRITE_ACCESS   (uint32_t)(0x00005555)
#define IWDG_REFRESH        (uint32_t)(0x0000AAAA)      //	Reset watchdog
#define IWDG_RELOAD         (300)                       //  ~2 sec.
#define IWDG_RESET          (IWDG->KR = IWDG_REFRESH)

#define ROLLBACK_VALUE      (uint16_t)400

//  Пункты меню:
enum MenuState {
    BUMP = 0,           //  Подача
    MOVE_AWAY,          //  Отвод штока
    MOVE_FORWARD,       //  Подвод штока
    SETTING_STEP_NUM,   //  Ввод количества шагов при подаче (доза)
    SETTING_DELAY       //  Ввод задержки перед подачей
};

//  Фазы движения толкателя:
enum FeedState {
    FEED_IDLE = 0,
    FEED_DELAY,
    FEED_FORWARD,
    FEED_ROLLBACK
};

//  Системные флаги:
typedef union {
    struct {
        enum     FeedState feed_state   :2;
        volatile uint32_t  btn_upd      :1;
        volatile uint32_t  ind_cnt      :3;
        volatile uint32_t  reserve      :6;
        volatile uint32_t  delay_set    :4;
        volatile uint32_t  steps_set    :16;
    };
    uint32_t value;
} FLAG_T;

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */

/*-------------------------------------------------------- Private variables: */
static volatile FLAG_T     flag;
static volatile uint16_t   Tick = 0;
static          BTN_SIGNAL btn;
static enum     MenuState  current_state  = BUMP;
static volatile uint8_t    steps_cursor = 2;
static volatile uint8_t    steps_buf[3] = {0};
static volatile uint16_t   delay_cnt = 0;
static volatile uint8_t    help_phase = 0;

/*---------------------------------------------- Private funcrion prototypes: */
void RCC_init(void);
void IWDG_init(void);
void SysTick_init(void);

void state_machine(BTN_SIGNAL btn);
void menu_screen(uint8_t signal);

/*----------------------------------------------------------------- Function: */
int main (void)
{
    RCC_init();
    IWDG_init();
    SysTick_init();

    init_display();
    init_button_pin();
    init_stepper();

    __enable_irq();

    stepper_set_speed(2);

    flag.steps_set = 1;
    steps_buf[0] = (flag.steps_set / 100);
    steps_buf[1] = (flag.steps_set % 100) / 10;
    steps_buf[2] = (flag.steps_set % 10);

    flag.feed_state = FEED_IDLE;

    while (1)
    {
        IWDG_RESET;

        //  опрос кнопок:
        if (flag.btn_upd) {
            flag.btn_upd = 0;
            btn.value = button_debounce();
        }

        //  Основной автомат:
        state_machine(btn);
        btn.value = 0x00;

        //  обновление дисплея:
        if (display_upd_get()) send_frame_to_display(0, menu_screen);
    }
}

/*-------------------------------------------- System initialization function */
void RCC_init(void)
{
    uint16_t tickstart;
    /* (1) Enable power interface clock */
    /* (2) Select voltage scale 1 (1.65V - 1.95V) i.e. (01)  for VOS bits in PWR_CR */
    /* (3) Enable HSI in RCC-> CR */
    /* (4) Wait for HSI ready flag */
    /* (5) Set PLL on HSI, multiply by 4 and divided by 2 */
    /* (6) Enable the PLL in RCC_CR register */
    /* (7) Wait for PLL ready flag */
    /* (8) Select PLL as system clock */
    /* (9) Wait for clock switched on PLL */
    RCC->APB1ENR |= (RCC_APB1ENR_PWREN);                                        /* (1) */
    PWR->CR = (PWR->CR & ~(PWR_CR_VOS)) | PWR_CR_VOS_0;                         /* (2) */
    RCC->CR |= RCC_CR_HSION;                                                    /* (3) */
    tickstart = Tick;
    while (!(RCC->CR & RCC_CR_HSIRDY))                                          /* (4) */
    {
        if ((Tick - tickstart) > TIMEOUT_VALUE) return;
    }
    RCC->CFGR |= RCC_CFGR_PLLSRC_HSI | RCC_CFGR_PLLMUL4 | RCC_CFGR_PLLDIV2;     /* (5) */
    RCC->CR |= RCC_CR_PLLON;                                                    /* (6) */
    tickstart = Tick;
    while (!(RCC->CR & RCC_CR_PLLRDY))                                          /* (7) */
    {
        if ((Tick - tickstart) > TIMEOUT_VALUE) return;
    }
    FLASH->ACR |= FLASH_ACR_LATENCY;
    FLASH->ACR |= FLASH_ACR_PRFTEN;

    RCC->CFGR |= RCC_CFGR_SW_PLL;                                               /* (8) */
    tickstart = Tick;
    while (!(RCC->CFGR & RCC_CFGR_SWS_PLL))                                     /* (9) */
    {
        if ((Tick - tickstart) > TIMEOUT_VALUE) return;
    }
}

void IWDG_init (void)
{
    RCC->CSR |= RCC_CSR_LSION;
    while(!(RCC->CSR & RCC_CSR_LSIRDY));

    IWDG->KR = IWDG_START;
    IWDG->KR = IWDG_WRITE_ACCESS;
    IWDG->PR = (IWDG_PR_PR_2 | IWDG_PR_PR_1 | IWDG_PR_PR_0);    //  divide to 256
    IWDG->RLR = IWDG_RELOAD;
    while(IWDG->SR)
    {
      /* add time out here for a robust application */
    }
    IWDG->KR = IWDG_REFRESH;
}

void SysTick_init(void)
{
    SysTick->LOAD = 31999;
    SysTick->VAL  = 31999;
    NVIC_SetPriority(SysTick_IRQn, 7);
    SysTick->CTRL =(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk);
}

/*-------------------------------------------------- System interrupt handler */

void NMI_Handler(void)
{
    while(1);
}

void SysTick_Handler(void)
{
    //  Отсчет задержки запуска:
    if (delay_cnt > 0) delay_cnt--;

    //  Опрос кнопок:
    static uint8_t btn_cnt = 0;
    if (btn_cnt < 9) {
        btn_cnt++;
    } else {
        btn_cnt = 0;
        flag.btn_upd = 1;
    }

    //  Обновление дисплея:
    static uint8_t display_cnt = 0;
    if (display_cnt < 124) {
        display_cnt++;
    } else {
        display_cnt = 0;
        display_upd_en();
        flag.ind_cnt++;
        if (!flag.ind_cnt) help_phase++;
    }

    //  Шаг двигателя:
    stepper_handler();
}

/*----------------------------------------------------------- User functions: */
void state_machine (BTN_SIGNAL btn)
{
    switch (current_state) {
    case BUMP:
        switch (flag.feed_state) {
        case FEED_IDLE:
            if (btn.a_release) {
                current_state = MOVE_AWAY;
            } else if (btn.b_release || btn.b_long || btn.a_long) {
                delay_cnt = flag.delay_set * 100;
                flag.ind_cnt = 0;
                flag.feed_state = FEED_DELAY;
            }
            break;
        case FEED_DELAY:
            if (delay_cnt == 0) {
                stepper_start((int32_t)(flag.steps_set + ROLLBACK_VALUE));
                flag.feed_state = FEED_FORWARD;
            }
            break;
        case FEED_FORWARD:
            if (stepper_get_state() == 0) {
                stepper_start((int32_t)(0 - ROLLBACK_VALUE));
                flag.feed_state = FEED_ROLLBACK;
            }
            break;
        case FEED_ROLLBACK:
            if (stepper_get_state() == 0) flag.feed_state = FEED_IDLE;
            break;
        };
        break;
    case MOVE_AWAY:
        if (btn.a_release) {
            stepper_stop();
            current_state = MOVE_FORWARD;
        } else if (btn.b_release || btn.b_long || btn.a_long) {
            if (stepper_get_state()) {
                stepper_stop();
            } else {
                stepper_start((int32_t)(0 - (2048 * 200)));
            }
        }
        break;
    case MOVE_FORWARD:
        if (btn.a_release) {
            stepper_stop();
            current_state = SETTING_STEP_NUM;
        } else if (btn.b_release || btn.b_long || btn.a_long) {
            if (stepper_get_state()) {
                stepper_stop();
            } else {
                stepper_start((int32_t)(2048 * 200));
            }
        }
        break;
    case SETTING_STEP_NUM:
        if (btn.a_long || btn.b_long) (steps_cursor > 0) ? (steps_cursor--) : (steps_cursor = 2);
        if (btn.b_release) (steps_buf[steps_cursor] < 9) ? (steps_buf[steps_cursor]++) : (steps_buf[steps_cursor] = 0);
        flag.steps_set = (steps_buf[0] * 100) + (steps_buf[1] * 10) + steps_buf[2];
        if (btn.a_release) {
            if (flag.steps_set == 0) {
                flag.steps_set = 1;
                steps_buf[2] = 1;
            }
            steps_cursor = 2;
            current_state = SETTING_DELAY;
        }
        break;
    case SETTING_DELAY:
        if (btn.a_release) current_state = BUMP;
        if (btn.b_release) (flag.delay_set < 15) ? (flag.delay_set++) : (flag.delay_set = 0);
        break;
    };
}



void menu_screen (uint8_t signal)
{
    frame_font_select(FONT_NORMAL);

    frame_text_string( 0, (56 - (BUMP << 3)), "ПOДAЧA:");
    frame_text_string(94, (56 - (BUMP << 3)), ((flag.feed_state == FEED_IDLE) ? "ПУCK" : ((flag.ind_cnt < 6) ? "   /!\\" : " ")));

    frame_text_string( 0, (54 - (MOVE_AWAY << 3)), "OTBOД ШTOKA:");
    frame_text_string(94, (54 - (MOVE_AWAY << 3)), (((current_state == MOVE_AWAY) && (stepper_get_state())) ? ((flag.ind_cnt < 6) ? "СTOП" : " ") : "ПУCK"));

    frame_text_string( 0, (52 - (MOVE_FORWARD << 3)), "ПOДBOД ШTOKA:");
    frame_text_string(94, (52 - (MOVE_FORWARD << 3)), (((current_state == MOVE_FORWARD) && (stepper_get_state())) ? ((flag.ind_cnt < 6) ? "СTOП" : " ") : "ПУCK"));

    frame_text_string( 0, (50 - (SETTING_STEP_NUM << 3)), "ЧИCЛO ШAГOB:");
    if (current_state == SETTING_STEP_NUM) {
        for (uint8_t i = 0; i < 3; i++) {
            if (i == steps_cursor) {
                if (flag.ind_cnt < 6) frame_text_char((((steps_buf[i] == 1) ? 96 : 95) + i * 8), (50 - (SETTING_STEP_NUM << 3)), (steps_buf[i] + '0'));
            } else {
                frame_text_char((((steps_buf[i] == 1) ? 96 : 95) + i * 8), (50 - (SETTING_STEP_NUM << 3)), (steps_buf[i] + '0'));
            }
        }
    } else {
        for (uint8_t i = 0; i < 3; i++) frame_text_char((((steps_buf[i] == 1) ? 96 : 95) + i * 8), (50 - (SETTING_STEP_NUM << 3)), (steps_buf[i] + '0'));
    }
    frame_text_string(  0, (48 - (SETTING_DELAY << 3)), "ЗAДEPЖKA:");
    frame_text_string(104, (48 - (SETTING_DELAY << 3)), "CEK.");
    frame_text_number( 88, (48 - (SETTING_DELAY << 3)), (int32_t)flag.delay_set, 1);

    //  Курсор:
    frame_text_char( 82, (56 - (current_state * 10)), '[');
    frame_text_char(125, (56 - (current_state * 10)), ']');

    //  Подсказка:
    frame_draw_line(0, 11, 127, 11);
    switch (current_state) {
    case BUMP:
    case MOVE_AWAY:
    case MOVE_FORWARD:
        if (help_phase >= 4) help_phase = 0;
        if ((help_phase == 0) || (help_phase == 1)) {
            frame_text_string(0, 0, "ПУCK / CTOП:     HAЖATИE  [B]");
        } else {
            frame_text_string(16, 0, "УДEPЖAHИE  [A]  ИЛИ  [B]");
        }
        break;
    case SETTING_STEP_NUM:
        if (help_phase >= 4) help_phase = 0;
        if ((help_phase == 0) || (help_phase == 1)) {
            frame_text_string(0, 0, "ИHKPEMEHT:      HAЖATИE  [B]");
        } else {
            frame_text_string(0, 0, "PAЗPЯД:  УДEPЖ.  [A] ИЛИ [B]");
        }
        break;
    case SETTING_DELAY:
        if (help_phase >= 6) help_phase = 0;
        if ((help_phase == 0) || (help_phase == 1)) {
            frame_text_string(12, 0, "ПAУЗA ПEPEД ПOДAЧEЙ");
        } else if ((help_phase == 2) || (help_phase == 3)) {
            frame_text_string(0, 0, "ИHKPEMEHT:      HAЖATИE  [B]");
        } else {
            frame_text_string(30, 0, "BEPCИЯ ПO:  1.3");
        }
        break;
    }
}
