/**
 * @file		stepper.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		29.11.2022
 * @brief
 */

/*----------------------------------------------------------------- Includes: */
#include "stepper.h"

/*------------------------------------------------------------------ Defines: */
/*
//  Orange:
#define PHASE_A_SET      GPIO_BSRR_BS_0
#define PHASE_A_RESET    GPIO_BSRR_BR_0
//  Yellow:
#define PHASE_B_SET      GPIO_BSRR_BS_1
#define PHASE_B_RESET    GPIO_BSRR_BR_1
//  Pink:
#define PHASE_C_SET      GPIO_BSRR_BS_4
#define PHASE_C_RESET    GPIO_BSRR_BR_4
//  Blue:
#define PHASE_D_SET      GPIO_BSRR_BS_7
#define PHASE_D_RESET    GPIO_BSRR_BR_7
*/
#define PHASE_RESET_ALL (uint32_t)(GPIOA->BRR = (GPIO_BRR_BR_0 | GPIO_BRR_BR_1 | GPIO_BRR_BR_4 | GPIO_BRR_BR_7))

typedef union {
    struct {
        uint8_t speed      :4;     //  число отсчетов на один шаг.
        uint8_t phase      :3;     //  текущий шаг, от 0 до 7.
        uint8_t busy       :1;     //  0 - свободен, 1 - в работе.
    };
    uint8_t value;
} STEPPER_T;

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */
STEPPER_T stepper;

/*-------------------------------------------------------- Private variables: */
static uint8_t speed_cnt = 0;
static int32_t step_cnt  = 0;

/*----------------------------------------------- Public function prototypes: */

/*---------------------------------------------- Private function prototypes: */

/*----------------------------------------------------------------- Function: */
void init_stepper (void)
{
    RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
    GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE0)) | (GPIO_MODER_MODE0_0);     //  A, PA0
    GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE1)) | (GPIO_MODER_MODE1_0);     //  B, PA1
    GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE4)) | (GPIO_MODER_MODE4_0);     //  C, PA4
    GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE7)) | (GPIO_MODER_MODE7_0);     //  D, PA7
    PHASE_RESET_ALL;

    stepper.speed = 2;
    stepper.phase = 0;
    stepper.busy  = 0;
}



void stepper_set_speed (uint8_t speed)
{
    if (stepper.busy) return;
    if (speed <  1) speed =  1;
    if (speed > 16) speed = 16;
    stepper.speed = speed - 1;
}



void stepper_start (int32_t step_num)
{
    if (stepper.busy) return;
    step_cnt = step_num;
    speed_cnt = 0;
    stepper.busy = 1;
}



void stepper_stop (void)
{
    while (speed_cnt);
    stepper.busy = 0;
    step_cnt = 0;
    PHASE_RESET_ALL;
}



uint8_t stepper_get_state (void)
{
    return (uint8_t)stepper.busy;
}



uint8_t stepper_get_speed (void)
{
    return (uint8_t)(stepper.speed + 1);
}



void stepper_handler (void)
{
    if (stepper.busy) {
        if (speed_cnt < stepper.speed) {
            speed_cnt++;
        } else {
            speed_cnt = 0;
            if (step_cnt > 0) {
                step_cnt--;
                (stepper.phase > 0) ? (stepper.phase--) : (stepper.phase = 3);
            } else if (step_cnt < 0) {
                step_cnt++;
                (stepper.phase < 3) ? (stepper.phase++) : (stepper.phase = 0);
            } else {
                PHASE_RESET_ALL;
                stepper.busy = 0;
                return;
            }
            switch (stepper.phase) {
/*
            //  Однофазное управление:
            case 0: GPIOA->BSRR = (GPIO_BSRR_BS_0 | GPIO_BSRR_BR_1 | GPIO_BSRR_BR_4 | GPIO_BSRR_BR_7); break;
            case 1: GPIOA->BSRR = (GPIO_BSRR_BR_0 | GPIO_BSRR_BS_1 | GPIO_BSRR_BR_4 | GPIO_BSRR_BR_7); break;
            case 2: GPIOA->BSRR = (GPIO_BSRR_BR_0 | GPIO_BSRR_BR_1 | GPIO_BSRR_BS_4 | GPIO_BSRR_BR_7); break;
            case 3: GPIOA->BSRR = (GPIO_BSRR_BR_0 | GPIO_BSRR_BR_1 | GPIO_BSRR_BR_4 | GPIO_BSRR_BS_7); break;

            //  Двухфазное управление:
            case 0: GPIOA->BSRR = (GPIO_BSRR_BR_0 | GPIO_BSRR_BR_1 | GPIO_BSRR_BS_4 | GPIO_BSRR_BS_7); break;
            case 1: GPIOA->BSRR = (GPIO_BSRR_BR_0 | GPIO_BSRR_BS_1 | GPIO_BSRR_BS_4 | GPIO_BSRR_BR_7); break;
            case 2: GPIOA->BSRR = (GPIO_BSRR_BS_0 | GPIO_BSRR_BS_1 | GPIO_BSRR_BR_4 | GPIO_BSRR_BR_7); break;
            case 3: GPIOA->BSRR = (GPIO_BSRR_BS_0 | GPIO_BSRR_BR_1 | GPIO_BSRR_BR_4 | GPIO_BSRR_BS_7); break;
 */
            //  Двухфазное управление, реверс для платы 1.0:
            case 0: GPIOA->BSRR = (GPIO_BSRR_BS_0 | GPIO_BSRR_BR_1 | GPIO_BSRR_BR_4 | GPIO_BSRR_BS_7); break;
            case 1: GPIOA->BSRR = (GPIO_BSRR_BS_0 | GPIO_BSRR_BS_1 | GPIO_BSRR_BR_4 | GPIO_BSRR_BR_7); break;
            case 2: GPIOA->BSRR = (GPIO_BSRR_BR_0 | GPIO_BSRR_BS_1 | GPIO_BSRR_BS_4 | GPIO_BSRR_BR_7); break;
            case 3: GPIOA->BSRR = (GPIO_BSRR_BR_0 | GPIO_BSRR_BR_1 | GPIO_BSRR_BS_4 | GPIO_BSRR_BS_7); break;
            };
        }
    } else {
        PHASE_RESET_ALL;
    }
}
