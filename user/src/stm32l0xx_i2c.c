/**
 * @file		stm32l0_i2c.c
 * @author		Chugay E.A.
 * @version		0.01
 * @date		21.03.2022
 * @brief		Работа с интерфейсом I2C МК STM32L011D3P6.
 */

/*----------------------------------------------------------------- Includes: */
#include "stm32l0xx_i2c.h"

/*------------------------------------------------------------------ Defines: */

/*--------------------------------------------------------- Extern variables: */

/*--------------------------------------------------------- Public variables: */

/*-------------------------------------------------------- Private variables: */
static uint8_t *i2c1_tx_data = 0;
static uint8_t  i2c1_tx_cnt  = 0;
static uint8_t *i2c1_rx_data = 0;
static uint8_t  i2c1_rx_cnt  = 0;

/*----------------------------------------------- Public function prototypes: */

/*---------------------------------------------- Private function prototypes: */

/*----------------------------------------------------------------- Function: */
void I2C1_init (void)
{
    /* Enable the peripheral clock of GPIOA */
    RCC->IOPENR |= RCC_IOPENR_GPIOAEN;
    /* (1) PU for I2C signals */
    /* (2) open drain for I2C signals */
    /* (3) AF1 for I2C signals */
    /* (4) Select AF mode (10) on PB9 and PB10 */
    GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPD9   | GPIO_PUPDR_PUPD10);          /* (1) */
    GPIOA->PUPDR |=  (GPIO_PUPDR_PUPD9_0 | GPIO_PUPDR_PUPD10_0);
    GPIOA->OTYPER |= (GPIO_OTYPER_OT_9   | GPIO_OTYPER_OT_10);          /* (2) */
    GPIOA->AFR[1] &= (GPIO_AFRH_AFSEL9   | GPIO_AFRH_AFSEL10);          /* (3) */
    GPIOA->AFR[1] |= (0x00000110);
    GPIOA->MODER &= ~(GPIO_MODER_MODE9   | GPIO_MODER_MODE10);          /* (4) */
    GPIOA->MODER |=  (GPIO_MODER_MODE9_1 | GPIO_MODER_MODE10_1);

    //  I2C1 init:
    RCC->APB1ENR |=  RCC_APB1ENR_I2C1EN;
    RCC->CCIPR   &= ~RCC_CCIPR_I2C1SEL;
    I2C1->TIMINGR = (uint32_t)0x20302E37;
    I2C1->CR1 |= I2C_CR1_PE;
//  I2C1->CR1 |= (I2C_CR1_PE | I2C_CR1_STOPIE);
    I2C1->CR2 |= I2C_CR2_AUTOEND;
    NVIC_SetPriority(I2C1_IRQn, 8);
    NVIC_EnableIRQ(I2C1_IRQn);
}



void I2C1_write (uint8_t addr_slave, uint8_t addr_word, uint8_t size, uint8_t *data)
{
    while (I2C1->ISR & I2C_ISR_BUSY);

    i2c1_tx_cnt = 0;
    i2c1_tx_data = data;
    I2C1->CR2 &= ~((0xFF << 16) | (0x7F << 1) | I2C_CR2_RD_WRN);

    I2C1->CR1 &= ~I2C_CR1_RXIE;
    while (I2C1->CR1 & I2C_CR1_RXIE);       //  DEBUG
    I2C1->CR1 |= I2C_CR1_TXIE;
    while (!(I2C1->CR1 & I2C_CR1_TXIE));    //  DEBUG
    I2C1->CR2 |= ((size + 1) << 16) | (addr_slave << 1);
    I2C1->TXDR = addr_word;
    I2C1->CR2 |= I2C_CR2_START;     //  генерируем "старт"
}



void I2C1_read (uint8_t addr_slave, uint8_t addr_word, uint8_t size, uint8_t *data)
{
    while (I2C1->ISR & I2C_ISR_BUSY);

    i2c1_rx_cnt = 0;
    i2c1_rx_data = data;
    I2C1->CR2 &= ~((0xFF << 16) | (0x7F << 1) | I2C_CR2_RD_WRN);

    I2C1->CR1 &= ~I2C_CR1_TXIE;
    while (I2C1->CR1 & I2C_CR1_TXIE);       //  DEBUG
    I2C1->CR1 |= I2C_CR1_RXIE;
    while (!(I2C1->CR1 & I2C_CR1_RXIE));    //  DEBUG
    I2C1->CR2 |= (size << 16) | (addr_slave << 1) | I2C_CR2_RD_WRN;
    I2C1->CR2 |= I2C_CR2_START;     //  генерируем "старт"
}



/*-------------------------------------------------------- Interrupt handler: */
void I2C1_IRQHandler(void)
{
    uint32_t I2C_InterruptStatus = I2C1->ISR;

    if (I2C_InterruptStatus & I2C_ISR_TXIS) {
        I2C1->TXDR = (uint8_t)i2c1_tx_data[i2c1_tx_cnt++];
    }
    if (I2C_InterruptStatus & I2C_ISR_RXNE) {
        i2c1_rx_data[i2c1_rx_cnt++] = I2C1->RXDR;
    }
/*
    if (I2C_InterruptStatus & I2C_ISR_STOPF) {
        I2C1->ICR |= I2C_ICR_STOPCF;
    }
*/
}
